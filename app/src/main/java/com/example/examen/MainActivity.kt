package com.example.examen

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AlertDialog.Builder
import kotlinx.android.synthetic.main.activity_main.*
import android.os.CountDownTimer
import android.util.Log
import android.view.animation.AnimationUtils
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var ponitsgame: TextView
    private lateinit var instructionsgame: TextView

    private lateinit var startButton: Button
    private lateinit var tapButtom: Button

    private var timeLeft = 10
    private var gameScore = 0


    private var isGameStarted = false


    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        startButton = findViewById(R.id.button_start)
        tapButtom = findViewById(R.id.button_tap)

        ponitsgame = findViewById(R.id.text_life)
        instructionsgame = findViewById(R.id.text_instrucction)

        this.startButton.setOnClickListener {button ->
            isGameStarted=true;
        }


        this.tapButtom.setOnClickListener{ button ->
            incrementScore()
        }


    }

    private fun incrementScore(){
        if(!isGameStarted){
            startGame()
        }

        Log.d(TAG,"DEBE INCREMENTAR")

        gameScore = gameScore +50;
        ponitsgame.text= getString(R.string.your_life_is,gameScore)

    }


    private fun startGame(){

    }
}
